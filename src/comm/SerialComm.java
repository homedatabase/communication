package comm;

import java.io.InputStream;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class SerialComm {


	public SerialComm(String portname, int baudrate, int databits, int stopbits, int parity) {
		setPortname(portname);
		setBaudrate(baudrate);
		setDatabits(databits);
		setStopbits(stopbits);
		setParity(parity);
	}

	private String portname;
	private int baudrate;
	private int databits;
	private int stopbits;
	private int parity;

	private CommPort commPort = null;
	private CommPortIdentifier portIdentifier = null;
	private boolean channelOpen = false;

	public SerialPort serialPort = null;
	public InputStream in = null;

	public String getPortname() {
		return portname;
	}

	public void setPortname(String portname) {
		this.portname = portname;
	}

	public int getBaudrate() {
		return baudrate;
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
	}

	public int getDatabits() {
		return databits;
	}

	public void setDatabits(int databits) {
		this.databits = databits;
	}

	public int getStopbits() {
		return stopbits;
	}

	public void setStopbits(int stopbits) {
		this.stopbits = stopbits;
	}

	public int getParity() {
		return parity;
	}

	public void setParity(int parity) {
		this.parity = parity;
	}

	public boolean getOpenState() {
		return channelOpen;
	}

	public void openChannel() throws Exception {
		channelOpen = connect();
	}

	public void closeChannel() {
		channelOpen = disconnect();
	}

	private boolean connect() throws Exception {
		portIdentifier = CommPortIdentifier.getPortIdentifier(portname);
		if (!portIdentifier.isCurrentlyOwned()) {
			commPort = portIdentifier.open(this.getClass().getName(), 2000);

			if (commPort instanceof SerialPort) {
				serialPort = (SerialPort) commPort;
				serialPort.setSerialPortParams(baudrate, databits, stopbits, parity);
				serialPort.notifyOnDataAvailable(true);

			} else {
				commPort.close();
			}
		}
		return portIdentifier.isCurrentlyOwned();

	}

	private boolean disconnect() {
		if (commPort != null) {
			commPort.close();
		}
		if (portIdentifier != null) {
			return portIdentifier.isCurrentlyOwned();
		} else {
			return false;
		}
	}
}
