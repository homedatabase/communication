package comm;

import java.util.ArrayList;

import gnu.io.CommPortIdentifier;

public class PortAv {

	static ArrayList<String> portnames = new ArrayList<String>();
	private static CommPortIdentifier portIdentifier;

	public PortAv() {
	}

	public static ArrayList<String> listPorts() {
		portnames.clear();
		java.util.Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			portIdentifier = portEnum.nextElement();
			if (portIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (!portIdentifier.isCurrentlyOwned()) {
					portnames.add(portIdentifier.getName());
				}
			}
		}

		return portnames;
	}

}
