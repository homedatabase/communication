package gui;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import comm.PortAv;
import dev.GazDevice;
import dev.GazDevices;
import dev.ShowDevice;
import gnu.io.SerialPort;
import model.ParityType;
import model.TimeRecord;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import java.awt.GridLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JFormattedTextField;

public class GazDialog extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	private ShowDevice showDevice = null;

	private JButton btnActivate = new JButton("Aktivieren");
	private JButton btnDeactivate = new JButton("Deaktivieren");
	private final JPanel panel = new JPanel();
	private final JComboBox<GazDevice> comboGaz = new JComboBox<GazDevice>();
	private final JLabel lblClock = new JLabel("Anzeigen");
	private final JButton btnOk = new JButton("OK");
	private final JPanel portPanel = new JPanel();
	private final JLabel lblPort = new JLabel("Schnittstelle");
	private final JComboBox<String> comboPort = new JComboBox<String>();
	private final JLabel lblBaudrate = new JLabel("Baudrate");
	private final JComboBox<Integer> comboBaud = new JComboBox<Integer>();
	private final ButtonGroup btnGroupDatabits = new ButtonGroup();
	private final ButtonGroup btnGroupStopbits = new ButtonGroup();
	private final JComboBox<ParityType> comboParity = new JComboBox<ParityType>();
	private final JPanel stopPanel = new JPanel();
	private final JPanel dataPanel = new JPanel();
	private JButton btnSend = new JButton("Send");
    private MaskFormatter timeFormat = new MaskFormatter("##:##:##.##");
	private JFormattedTextField fmtTxt = new JFormattedTextField(timeFormat);
	private final JRadioButton rdbtnStopbit_1 = new JRadioButton("1 Stopbit");
	private final JRadioButton rdbtnStopbit_15 = new JRadioButton("1,5 Stopbits");
	private final JRadioButton rdbtnStopbit_2 = new JRadioButton("2 Stopbits");
	private final JRadioButton rdbtnBits_7 = new JRadioButton("7 Bits");
	private final JRadioButton rdbtnBits_8 = new JRadioButton("8 Bits");



	public ShowDevice getShowDevice() {
		return showDevice;
	}

	public GazDialog() throws ParseException {

		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 513, 308);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		panel.setBounds(5, 5, 310, 252);
		panel.setAlignmentX(Component.LEFT_ALIGNMENT);

		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		portPanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		portPanel.setAlignmentY(1.0f);

		panel.add(portPanel, BorderLayout.CENTER);
		portPanel.setLayout(null);
		lblPort.setBounds(15, 50, 69, 14);
		portPanel.add(lblPort);
		comboPort.setBounds(15, 70, 69, 23);
		comboPort.setPreferredSize(new Dimension(100, 23));
		comboPort.setMinimumSize(new Dimension(100, 23));
		portPanel.add(comboPort);
		lblBaudrate.setBounds(111, 50, 69, 14);
		portPanel.add(lblBaudrate);
		comboBaud.setModel(
				new DefaultComboBoxModel<Integer>(new Integer[] { 1200, 2400, 9600, 19200, 38400, 57600, 115200 }));
		comboBaud.setMinimumSize(new Dimension(50, 20));
		comboBaud.setPreferredSize(new Dimension(50, 20));
		comboBaud.setBounds(111, 71, 97, 20);
		portPanel.add(comboBaud);

		stopPanel.setBorder(new TitledBorder(null, "Stopbits", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		stopPanel.setBounds(111, 150, 183, 91);
		portPanel.add(stopPanel);
		stopPanel.setLayout(new GridLayout(0, 2, 0, 0));

		rdbtnStopbit_1.setActionCommand("SB1");
		btnGroupStopbits.add(rdbtnStopbit_1);
		rdbtnStopbit_1.setSelected(true);
		stopPanel.add(rdbtnStopbit_1);

		rdbtnStopbit_15.setActionCommand("SB15");
		btnGroupStopbits.add(rdbtnStopbit_15);
		stopPanel.add(rdbtnStopbit_15);

		rdbtnStopbit_2.setActionCommand("SB2");
		btnGroupStopbits.add(rdbtnStopbit_2);
		stopPanel.add(rdbtnStopbit_2);

		dataPanel.setBorder(new TitledBorder(null, "Databits", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		dataPanel.setBounds(10, 150, 91, 91);
		portPanel.add(dataPanel);
		dataPanel.setLayout(new GridLayout(0, 1, 0, 0));

		btnGroupDatabits.add(rdbtnBits_8);
		rdbtnBits_8.setSelected(true);
		rdbtnBits_8.setActionCommand("DB8");
		dataPanel.add(rdbtnBits_8);

		rdbtnBits_7.setActionCommand("DB7");
		btnGroupDatabits.add(rdbtnBits_7);
		dataPanel.add(rdbtnBits_7);

		JLabel lblParitt = new JLabel("Parität");
		lblParitt.setHorizontalAlignment(SwingConstants.LEFT);
		lblParitt.setAlignmentX(Component.RIGHT_ALIGNMENT);
		lblParitt.setBounds(111, 104, 46, 14);
		portPanel.add(lblParitt);

		comboParity.setBounds(111, 119, 97, 20);
		portPanel.add(comboParity);
		comboGaz.setBounds(69, 8, 200, 23);
		portPanel.add(comboGaz);
		comboGaz.setPreferredSize(new Dimension(200, 23));
		comboGaz.setMinimumSize(new Dimension(500, 23));
		lblClock.setBounds(15, 12, 44, 14);
		portPanel.add(lblClock);
		lblClock.setHorizontalAlignment(SwingConstants.RIGHT);

		for (GazDevice gazDevice : GazDevices.getGazDeviceList()) {
		comboGaz.addItem(gazDevice);
		}
		comboGaz.addActionListener(this);
		comboGaz.setActionCommand("GazSelected");
		comboGaz.setSelectedIndex(0);
		
		btnOk.setBounds(382, 234, 105, 23);
		contentPane.add(btnOk);
		btnOk.setPreferredSize(new Dimension(100, 23));
		btnOk.setMinimumSize(new Dimension(100, 23));
		btnOk.setMaximumSize(new Dimension(200, 23));
		btnOk.addActionListener(this);
		btnOk.setActionCommand("ok");
		btnDeactivate.setBounds(382, 32, 105, 23);
		contentPane.add(btnDeactivate);

		btnDeactivate.setMaximumSize(new Dimension(200, 23));
		btnDeactivate.setMinimumSize(new Dimension(100, 23));
		btnDeactivate.setActionCommand("deactivate");
		btnDeactivate.addActionListener(this);
		btnDeactivate.setEnabled(false);
		btnActivate.setBounds(382, 5, 105, 23);
		contentPane.add(btnActivate);
		btnActivate.setPreferredSize(new Dimension(100, 23));

		btnActivate.setMaximumSize(new Dimension(200, 23));
		btnActivate.setMinimumSize(new Dimension(100, 23));
		btnActivate.setActionCommand("activate");
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Test", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(325, 81, 162, 66);
		contentPane.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		panel_1.add(btnSend, BorderLayout.SOUTH);
		btnSend.addActionListener(this);
		btnSend.setActionCommand("send");
		
	    timeFormat.setPlaceholderCharacter('0');
	    panel_1.add(fmtTxt, BorderLayout.CENTER);
		
		btnActivate.addActionListener(this);


		comboParity.addItem(ParityType.parNone());
		comboParity.addItem(ParityType.parEven());
		comboParity.addItem(ParityType.parOdd());
		comboParity.addItem(ParityType.parMark());
		
		addWindowListener(new WindowHandler());
	}


	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		switch (actionEvent.getActionCommand()) {
		case "activate":
			activateGaz();
			break;
		case "send":
			if (showDevice != null) {
				try {
					TimeRecord timerecord = new TimeRecord(0,0,fmtTxt.getText());
					showDevice.putString(timerecord.getTimeStr(TimeRecord.Format.LONGSTR));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
				
		case "deactivate":
			deactivateGaz();
			break;
		case "ok":
			setVisible(false);
			break;
		case "GazSelected":
			setComParameters((GazDevice) comboGaz.getSelectedItem());
			break;

		default:
			break;
		}
	}
	
	private void setComParameters(GazDevice gazDevice) {
		// set BaudRate
		DefaultComboBoxModel<Integer> modelBaud = (DefaultComboBoxModel<Integer>) comboBaud.getModel();
		comboBaud.setSelectedIndex(modelBaud.getIndexOf(gazDevice.getBaudrate()));

		// set Parity
		DefaultComboBoxModel<ParityType> modelPar = (DefaultComboBoxModel<ParityType>) comboParity.getModel();
		int idx = -1;
		for (int i = 0; i < modelPar.getSize(); i++) {
			if (modelPar.getElementAt(i).getParName() == gazDevice.getParity().toString()) {
				idx = i;
			}
		}
		comboParity.setSelectedIndex(idx);

		// set DataBits
		switch (gazDevice.getDatabits()) {
		case SerialPort.DATABITS_7:
			rdbtnBits_7.setSelected(true);
			break;
		default:
			rdbtnBits_8.setSelected(true);
			break;
		}
			// set StopBits
			switch (gazDevice.getStopbits()) {
			case SerialPort.STOPBITS_1:
				rdbtnStopbit_1.setSelected(true);
				break;
			case SerialPort.STOPBITS_1_5:
				rdbtnStopbit_15.setSelected(true);
				break;
			case SerialPort.STOPBITS_2:
				rdbtnStopbit_2.setSelected(true);
				break;
			default:
				rdbtnStopbit_1.setSelected(true);
				break;
			
		}
	}


	private void activateGaz() {
		try {
			// Get Clock Name
			GazDevice gazDevice = (GazDevice) comboGaz.getSelectedItem();
			String gazName = gazDevice.getShortName();

			// Get Port Name
			String portName = comboPort.getSelectedItem().toString();
			int baudrate = (int) comboBaud.getSelectedItem();

			// Get Databits
			String dbActionCommand = btnGroupDatabits.getSelection().getActionCommand();
			int databits;
			switch (dbActionCommand) {
			case "DB8":
				databits = SerialPort.DATABITS_8;
				break;
			case "DB7":
				databits = SerialPort.DATABITS_7;
				break;
			default:
				databits = SerialPort.DATABITS_8;
				break;
			}

			//Get Stopbits
			String sbActionCommand = btnGroupStopbits.getSelection().getActionCommand();
			int stopbits;
			switch (sbActionCommand) {
			case "SB1":
				stopbits = SerialPort.STOPBITS_1;
				break;
			case "SB15":
				stopbits = SerialPort.STOPBITS_1_5;
				break;
			case "SB2":
				stopbits = SerialPort.STOPBITS_2;
			default:
				stopbits = SerialPort.STOPBITS_1;
				break;
			}
			
			// Get Parity
			ParityType parityType = (ParityType) comboParity.getSelectedItem();
			int parity = parityType.getParity();

			// initialize Clock Device
			showDevice = new ShowDevice(gazName, portName, baudrate, databits, stopbits, parity);
			enableControls(false);
		
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Anschluss nicht verfügbar", "Uhr-Anschluss",
					JOptionPane.ERROR_MESSAGE);
		}

	}
	
	private void enableControls(boolean enabled) {
		btnActivate.setEnabled(enabled);
		btnDeactivate.setEnabled(!enabled);
		portPanel.setEnabled(false);
		Component[] com = portPanel.getComponents();
		for (int a = 0; a < com.length; a++) {
		     com[a].setEnabled(enabled);
		}
		com = stopPanel.getComponents();
		for (int a = 0; a < com.length; a++) {
		     com[a].setEnabled(enabled);
		}
		com = dataPanel.getComponents();
		for (int a = 0; a < com.length; a++) {
		     com[a].setEnabled(enabled);
		}
	}

	private void deactivateGaz() {
		if (showDevice != null) {
			showDevice.serialComm.closeChannel();
		}
		enableControls(true);
	}
	
	class WindowHandler extends WindowAdapter
	{
	    public void windowActivated(WindowEvent e) 
	    {
			ArrayList<String> portNames = PortAv.listPorts();
			for (String name : portNames) {
				comboPort.addItem(name.toString());
			}

	    }
	}

}


