package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JFrame;

import dev.ChronoDevices;
import dev.ClockDevice;
import dev.ShowDevice;
import dev.SendEventListener;
import dev.TimeEventListener;
import model.TimeRecord;

import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.BoxLayout;
import java.awt.Font;
import javax.swing.ButtonGroup;

public class MainGui implements ActionListener, TimeEventListener, SendEventListener {

	private JFrame frame;
	private JTextArea textArea = new JTextArea();
	private ClockDevice clockDevice = null;
	private ShowDevice showDevice = null;
	private JMenuBar menuBar;
	private JMenu mnDatei;
	private JMenuItem mntmBeenden;
	private JMenu mnDevice;
	private JMenuItem mntmClock;
	private JMenuItem mntmGaz;
	private JSeparator separator;

	private ClockDialog clockDialog;
	private GazDialog gazDialog;
	private JPanel panel;
	private JPanel panel_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final JRadioButton rdbtnNothing = new JRadioButton("keine Ausgabe");
	private final JRadioButton rdbtnChrono = new JRadioButton("Zeiten von Uhr");
	private final JRadioButton rdbtnNow = new JRadioButton("Uhrzeit");
	private final JLabel lblControl = new JLabel("23:59:59");
	private final Timer timer = new Timer(1000, this);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui window = new MainGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGui() {
		// ArrayList<String> ports = PortAv.listPorts();
		try {
			initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws Exception
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException, Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		frame = new JFrame("Communication");
		frame.setSize(900, 600);
		frame.setLocationByPlatform(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		menuBar = new JMenuBar();
		panel.add(menuBar, BorderLayout.NORTH);

		mnDatei = new JMenu("Datei");
		menuBar.add(mnDatei);

		mntmBeenden = new JMenuItem("Beenden");
		mnDatei.add(mntmBeenden);
		mntmBeenden.addActionListener(this);
		mntmBeenden.setActionCommand("exit");

		mnDevice = new JMenu("Geräte");
		menuBar.add(mnDevice);

		mntmClock = new JMenuItem("Uhr");
		mnDevice.add(mntmClock);
		mntmClock.addActionListener(this);
		mntmClock.setActionCommand("showClockDialog");

		separator = new JSeparator();
		mnDevice.add(separator);

		mntmGaz = new JMenuItem("Großanzeige");
		mnDevice.add(mntmGaz);
		mntmGaz.addActionListener(this);
		mntmGaz.setActionCommand("showGazDialog");

		panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		JScrollPane scrollpane = new JScrollPane(textArea);
		scrollpane.setBounds(10, 29, 148, 500);
		panel_1.add(scrollpane);

		JLabel lblZeiten = new JLabel("Zeiten");
		lblZeiten.setBounds(10, 11, 46, 14);
		panel_1.add(lblZeiten);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Ausgabe auf Gro\u00DFanzeige", TitledBorder.LEADING, TitledBorder.TOP,
				null, null));
		panel_2.setBounds(184, 35, 227, 92);
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		buttonGroup.add(rdbtnNothing);
		buttonGroup.add(rdbtnChrono);
		buttonGroup.add(rdbtnNow);

		rdbtnNow.setSelected(true);
		panel_2.add(rdbtnNothing);
		panel_2.add(rdbtnChrono);
		panel_2.add(rdbtnNow);

		JLabel lblKontrolle = new JLabel("Kontrolle");
		lblKontrolle.setBounds(194, 148, 46, 14);
		panel_1.add(lblKontrolle);

		lblControl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblControl.setBounds(194, 163, 119, 20);
		panel_1.add(lblControl);

		// to place here to get right look&feel
		clockDialog = new ClockDialog();
		gazDialog = new GazDialog();

		timer.start();
		timer.setActionCommand("OneSecond");

	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {

		switch (actionEvent.getActionCommand()) {
		case "exit":
			System.exit(0);
			break;
		case "showClockDialog":
			showClockDialog();
			break;
		case "showGazDialog":
			showGazDialog();
			break;
		case "OneSecond":
			showTime();
		default:
			break;
		}
	}

	public void showClockDialog() {
		clockDialog.setLocationRelativeTo(frame);
		clockDialog.setVisible(true);
		clockDevice = clockDialog.getClockDevice();
		if (clockDevice != null) {
			clockDevice.addTimeEventListener(this);
		}
	}

	public void showGazDialog() {
		gazDialog.setLocationRelativeTo(frame);
		gazDialog.setVisible(true);
		showDevice = gazDialog.getShowDevice();
		if (showDevice != null) {
			showDevice.addSendEventListener(this);
		}

	}

	public void showTime() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		String timestr = (String) LocalTime.now().format(dtf);
		if (rdbtnNow.isSelected() && showDevice != null) {
			try {
				showDevice.putString(timestr);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void reciveTimeEvent(int newTimesCnt) {
		// TODO Auto-generated method stub
		int end = ChronoDevices.getTimeList().size();
		int start = end - newTimesCnt;
		TimeRecord timerecord;
		String putstr;

		for (int i = start; i < end; i++) {
			timerecord = ChronoDevices.getTimeList().get(i);
			textArea.append(String.format("%03d: ", timerecord.getSeqNum())
					+ timerecord.getTimeStr(TimeRecord.Format.LONGSTR) + "\n");
		}
		textArea.setCaretPosition(textArea.getDocument().getLength());
		if (rdbtnChrono.isSelected()) {
			if (showDevice != null) {
				try {
					timerecord = ChronoDevices.getTimeList().get(end - 1);
					putstr = timerecord.getTimeStr(TimeRecord.Format.LONGSTR);
					showDevice.putString(putstr);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void reciveSendEvent(String putstr) {
		// TODO Auto-generated method stub
		lblControl.setText(putstr);

	}

}
