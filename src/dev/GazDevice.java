package dev;

import model.ParityType;

public class GazDevice {

	private String longName;
	private String shortName;
	private int baudrate;
	private int databits;
	private int stopbits;
	private ParityType parity;

	public GazDevice(String longName, String shortName, int baudrate, int databits, int stopbits, ParityType parity) {
		setLongName(longName);
		setShortName(shortName);
		setBaudrate(baudrate);
		setDatabits(databits);
		setStopbits(stopbits);
		setParity(parity);
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public int getBaudrate() {
		return baudrate;
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
	}

	public int getDatabits() {
		return databits;
	}

	public void setDatabits(int databits) {
		this.databits = databits;
	}

	public int getStopbits() {
		return stopbits;
	}

	public void setStopbits(int stopbits) {
		this.stopbits = stopbits;
	}

	public ParityType getParity() {
		return parity;
	}

	public void setParity(ParityType parity) {
		this.parity = parity;
	}
	
	public String toString() {
		return this.longName;
	}

}
