package dev;

import java.util.ArrayList;

import gnu.io.SerialPort;
import model.ParityType;
import model.TimeRecord;

public class ChronoDevices {

	public static ArrayList<ChronoDevice> chronoDeviceList = new ArrayList<ChronoDevice>();
	public static ArrayList<TimeRecord> timeList = new ArrayList<TimeRecord>();

	public ChronoDevices() {
	}

	public static ArrayList<ChronoDevice> getChronoDeviceList() {

		ChronoDevice cp505 = new ChronoDevice("Tag Heuer CP505", "CP505", 1200, SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1, ParityType.parNone());
		chronoDeviceList.add(cp505);
		ChronoDevice cp520 = new ChronoDevice("Tag Heuer CP520", "CP520", 9600, SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1, ParityType.parNone());
		chronoDeviceList.add(cp520);
		ChronoDevice cp545 = new ChronoDevice("Tag Heuer CP545", "CP545", 9600, SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1, ParityType.parNone());
		chronoDeviceList.add(cp545);
		ChronoDevice rs6 = new ChronoDevice("Rallye Star VI", "RS6", 9600, SerialPort.DATABITS_7, 
				SerialPort.STOPBITS_1, ParityType.parEven());
		chronoDeviceList.add(rs6);

		return chronoDeviceList;

	}

	public static ArrayList<TimeRecord> getTimeList() {
		return timeList;
	}

	private String recdata = "";

	public int getTime(String shortName, String rawData) {

		String rawStr, timeStr;
		String strHH, strMM, strSS, strZH;
		String strSecNum;
		String strSrc;
		int seqNum = 0;
		int src = 0;
		int cnt = 0;

		recdata = recdata + rawData;

		switch (shortName) {
		case "CP505":
			while (recdata.length() >= 15) {
				if (recdata.indexOf((char) 0xFF) == 0) {
					if (recdata.indexOf((char) 0x0d) == 14) {
						seqNum = Integer.parseInt(recdata.substring(1, 4));
						src = Integer.parseInt(recdata.substring(4, 5));
						rawStr = recdata.substring(5, 14);
						strHH = rawStr.substring(0, 2);
						strMM = rawStr.substring(2, 4);
						strSS = rawStr.substring(4, 6);
						strZH = rawStr.substring(6, 8);

						timeStr = strHH + ":" + strMM + ":" + strSS + "." + strZH;

						TimeRecord timeRecord = new TimeRecord(seqNum, src, timeStr);
						timeList.add(timeRecord);
						cnt++;

						recdata = recdata.substring(15, recdata.length());
					}
				} else {
					recdata = recdata.substring(1, recdata.length());
				}
			}
			break;
		case "CP520":
			while (recdata.length() >= 31) {
				if (recdata.indexOf("T") == 0) {
					if (recdata.indexOf((char) 0x0d) == 30) {
						strSecNum = recdata.substring(7, 11);
						strSecNum = strSecNum.replaceAll("[^\\d.]", "");
						seqNum = Integer.parseInt(strSecNum);
						strSrc = recdata.substring(12, 14);
						strSrc = strSrc.replace("M", "");
						src = Integer.parseInt(strSrc);

						rawStr = recdata.substring(15, 28);
						rawStr = rawStr.replace(' ', '0');

						strHH = rawStr.substring(0, 2);
						strMM = rawStr.substring(3, 5);
						strSS = rawStr.substring(6, 8);
						strZH = rawStr.substring(9, 11);
						timeStr = strHH + ":" + strMM + ":" + strSS + "." + strZH;
						TimeRecord timeRecord = new TimeRecord(seqNum, src, timeStr);
						timeList.add(timeRecord);
						cnt++;

						recdata = recdata.substring(31, recdata.length());
					} else {
						recdata = recdata.substring(1, recdata.length());
					}

				}
			}

			break;
		case "CP545":
			while (recdata.length() >= 42) {
				if (recdata.indexOf((char) 'T') == 0) {
					if ((recdata.indexOf((char) 0x0d) == 41) && (recdata.indexOf((char) 0x0a) == 42)) {
						strSecNum = recdata.substring(8, 12);
						strSecNum = strSecNum.replaceAll("[^\\d.]", "");
						seqNum = Integer.parseInt(strSecNum);
						strSrc = recdata.substring(13, 15);
						strSrc = strSrc.replace("M", "");
						src = Integer.parseInt(strSrc);
						rawStr = recdata.substring(16, 28);
						strHH = rawStr.substring(0, 2);
						strMM = rawStr.substring(3, 5);
						strSS = rawStr.substring(6, 8);
						strZH = rawStr.substring(9, 11);

						timeStr = strHH + ":" + strMM + ":" + strSS + "." + strZH;

						TimeRecord timeRecord = new TimeRecord(seqNum, src, timeStr);
						timeList.add(timeRecord);
						cnt++;

						recdata = recdata.substring(43, recdata.length());

					} else {
						recdata = recdata.substring(1, recdata.length());
					}
				}
			}
			break;
		case "RS6":
			while (recdata.length() >= 14) {
				if (recdata.indexOf((char) 0x02) == 0) {
					if (recdata.indexOf((char) 0x03) == 13) {
						strSecNum = recdata.substring(2, 5);
						strSecNum = strSecNum.replaceAll("[^\\d.]", "");
						seqNum = Integer.parseInt(strSecNum);
						strSrc = recdata.substring(1, 2);
						strSrc = strSrc.replace("M", "");
						src = Integer.parseInt(strSrc);

						rawStr = recdata.substring(5, 13);

						strHH = rawStr.substring(0, 2);
						strMM = rawStr.substring(2, 4);
						strSS = rawStr.substring(4, 6);
						strZH = rawStr.substring(6, 8);

						timeStr = strHH + ":" + strMM + ":" + strSS + "." + strZH;
						TimeRecord timeRecord = new TimeRecord(seqNum, src, timeStr);
						timeList.add(timeRecord);
						cnt++;

						recdata = recdata.substring(14, recdata.length());

					} else {
						recdata = recdata.substring(1, recdata.length());
					}

				}
			}
			break;
		default:
			break;
		}

		return cnt;

	}

}
