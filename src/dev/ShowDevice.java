package dev;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import comm.SerialComm;

public class ShowDevice {

	public ShowDevice(String gazName, String portName, int baudrate, int databits, int stopbits, int parity)
			throws Exception {
		this.gazName = gazName;
		this.portName = portName;
		this.baudrate = baudrate;
		this.databits = databits;
		this.stopbits = stopbits;
		this.parity = parity;
		initialize();
	}


	private String gazName;
	private String portName;
	private int baudrate;
	private int stopbits;
	private int databits;
	private int parity;

	public SerialComm serialComm = null;
	private OutputStream outputStream = null;

	private void initialize() throws Exception {
		serialComm = new SerialComm(portName, baudrate, databits, stopbits, parity);

		serialComm.openChannel();

		if (serialComm.serialPort != null) {
			outputStream = serialComm.serialPort.getOutputStream();
		} else {
			throw new Exception();
		}
	}

	public void putString(String outstr) throws IOException {
		String putstr;
		putstr = GazDevices.formatOutString(gazName, outstr);
		byte[] s = putstr.getBytes();
		for (Byte b : s) {
			outputStream.write(b);
		}
		notifySendEventListener(putstr);
	}
	
	private ArrayList<SendEventListener> sendEventListenerList = new ArrayList<SendEventListener>();

	public void addSendEventListener(SendEventListener sendEventListener) {
		sendEventListenerList.add(sendEventListener);
	}

	private void notifySendEventListener(String sendstr) {
		for (SendEventListener sendEventListener : sendEventListenerList) {
			sendEventListener.reciveSendEvent(sendstr);
		}
	}

}
