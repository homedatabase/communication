package dev;

import java.util.ArrayList;

import gnu.io.SerialPort;
import model.ParityType;

public class GazDevices {

	public static ArrayList<GazDevice> gazDeviceList = new ArrayList<GazDevice>();

	public GazDevices() {
	}

	public static ArrayList<GazDevice> getGazDeviceList() {

		GazDevice alge = new GazDevice("ALGE GAZ", "ALGE", 2400, SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1, ParityType.parNone());
		gazDeviceList.add(alge);
		GazDevice led = new GazDevice("LED Anzeige", "LED", 9600, SerialPort.DATABITS_7,
				SerialPort.STOPBITS_2, ParityType.parEven());
		gazDeviceList.add(led);

		return gazDeviceList;

	}

	public static String formatOutString(String gazName, String s) {

		String strMM = "00";
		String strSS = "00";
		String strZH = "00";
		String outstr;

		if (s.length() == 8) {
			strMM = s.substring(0, 2);
			strSS = s.substring(3, 5);
			strZH = s.substring(6, 8);
		}
		if (s.length() == 11) {
			strMM = s.substring(3, 5);
			strSS = s.substring(6, 8);
			strZH = s.substring(9, 11);
		}

		// string st;
		// st = " 23:45:93.\r";

		outstr = "        " + strMM + ":" + strSS + ":" + strZH + ".\r";

		// TODO Auto-generated method stub
		return outstr;
	}

}
