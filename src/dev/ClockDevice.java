package dev;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import comm.SerialComm;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class ClockDevice implements SerialPortEventListener {

	public ClockDevice(String clockName, String portName, int baudrate, int databits, int stopbits, int parity)
			throws Exception {
		this.clockName = clockName;
		this.portName = portName;
		this.baudrate = baudrate;
		this.databits = databits;
		this.stopbits = stopbits;
		this.parity = parity;
		initialize();
	}


	private String clockName;
	private String portName;
	private int baudrate;
	private int stopbits;
	private int databits;
	private int parity;

	public SerialComm serialComm = null;
	private InputStream inputStream = null;
	private int newTimesCnt = 0;
	private ChronoDevices chronoDevices = new ChronoDevices();


	private void initialize() throws Exception {
		serialComm = new SerialComm(portName, baudrate, databits, stopbits, parity);

		serialComm.openChannel();

		if (serialComm.serialPort != null) {
			inputStream = serialComm.serialPort.getInputStream();
			serialComm.serialPort.addEventListener(this);
		} else {
			throw new Exception();
		}
	
	}

	@Override
	public void serialEvent(SerialPortEvent arg0) {
		int data;
		String rawData = "";
		try {
			while ((data = inputStream.read()) > -1) {
				rawData += (char) data;
			}
			newTimesCnt = chronoDevices.getTime(clockName, rawData);

			if (newTimesCnt > 0) {
				notifyTimeEventListener(newTimesCnt);
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private ArrayList<TimeEventListener> timeEventListenerList = new ArrayList<TimeEventListener>();

	public void addTimeEventListener(TimeEventListener timeEventListener) {
		if (!timeEventListenerList.contains(timeEventListener)) {
			timeEventListenerList.add(timeEventListener);
		}
	}

	private void notifyTimeEventListener(int newTimesCnt) {
		for (TimeEventListener timeEventListener : timeEventListenerList) {
			timeEventListener.reciveTimeEvent(newTimesCnt);
		}
	}

}
