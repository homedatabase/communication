package model;

public class TimeRecord {

	public enum Format {
		LONGSTR, SHORTSTR
	};

	private int seqNum = 0;
	private int source = 0;
	private long time;

	public TimeRecord(int num, int src, int h, int m, int s, int zh) {
		setSeqNum(num);
		setSource(src);
		setTime(h, m, s, zh);
	}

	public TimeRecord(int num, int src, String sh, String sm, String ss, String szh) {
		setSeqNum(num);
		setSource(src);
		setTime(sh, sm, ss, szh);
	}

	public TimeRecord(int num, int src, String timestr) {
		setSeqNum(num);
		setSource(src);
		setTime(timestr);
	}

	public TimeRecord() {
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public void setTime(int h, int m, int s, int zh) {
		this.time = int2time(h, m, s, zh);
	}

	public void setTime(String sh, String sm, String ss, String szh) {
		int h, m, s, zh;
		h = Integer.parseInt(sh);
		m = Integer.parseInt(sm);
		s = Integer.parseInt(ss);
		zh = Integer.parseInt(szh);
		this.time = int2time(h, m, s, zh);
	}

	public void setTime(String timestr) {
		this.time = str2time(timestr);
	}

	public int getSeqNum() {
		return seqNum;
	}

	public int getSource() {
		return source;
	}

	public String getTimeStr(Format format) {
		int h, m, s, zh;
		long time = this.time;
		String timeStr = "0.00";
		h = (int) time / 360000;
		time = time - (360000 * h);
		m = (int) time / 6000;
		time = time - (6000 * m);
		s = (int) time / 100;
		time = time - (100 * s);
		zh = (int) time;

		switch (format) {
		case LONGSTR:
			timeStr = String.format("%02d:%02d:%02d.%02d", h, m, s, zh);
			break;
		case SHORTSTR:
			if (h == 0 && m == 0 && s == 0) {
				timeStr = String.format("0.%02d", zh);
			} else {
				if (h == 0 && m == 0) {
					timeStr = String.format("%02d.%02d", s, zh);
				} else {
					if (h == 0) {
						timeStr = String.format("%02d:%02d.%02d", m, s, zh);
					} else {
						timeStr = String.format("%02d:%02d:%02d.%02d", h, m, s, zh);
					}
				}
			}
		}
		return timeStr;
	}

	private long int2time(int h, int m, int s, int zh) {
		long time;
		if (zh > 100) {
			zh = zh / 10; // Operator int --> Ergebnis int
		}

		time = h * 360000 + m * 6000 + s * 100 + zh;
		return time;
	}

	private long str2time(String timestr) {
		int h = 0;
		int m = 0;
		int s = 0;
		int z = 0;
		int len = timestr.length();

		if (len == 11) {
			h = Integer.valueOf(timestr.substring(0, 2));
			m = Integer.valueOf(timestr.substring(3, 5));
			s = Integer.valueOf(timestr.substring(6, 8));
			z = Integer.valueOf(timestr.substring(9, 11));
		}
		return int2time(h, m, s, z);
	}

}
