package model;

import gnu.io.SerialPort;

public class ParityType {

	public ParityType(String parName, int parity) {
		setParity(parity);
		setParName(parName);
	}

	public String toString() {
		return parName;
	}

	public int getParity() {
		return parity;
	}

	public void setParity(int parity) {
		this.parity = parity;
	}

	public String getParName() {
		return parName;
	}

	public void setParName(String parName) {
		this.parName = parName;
	}

	private int parity;
	private String parName;

	static public ParityType parNone() {
		return new ParityType("None", SerialPort.PARITY_NONE);
	}

	static public ParityType parEven() {
		return new ParityType("Even", SerialPort.PARITY_EVEN);
	}

	static public ParityType parOdd() {
		return new ParityType("Odd", SerialPort.PARITY_ODD);
	}

	static public ParityType parMark() {
		return new ParityType("Mark", SerialPort.PARITY_MARK);
	}

}
